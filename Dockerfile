FROM node:16.13.1

RUN apt-get update \
    && apt upgrade -y \
    && apt-get install procps curl jq -y \
    && apt-get clean

RUN mkdir /home/app

WORKDIR /home/app

COPY package.json ./

RUN npm i

COPY ./ ./

CMD ["node","index.js"]
