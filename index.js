const Koa = require("koa");
const koaRouter = require('koa-router');
const mongoose = require('mongoose');
const bodyParser = require('koa-bodyparser');
const dotenv = require('dotenv');
const validation = require("./models/validation")

dotenv.config();

const app = new Koa();
const router = new koaRouter();

app
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

//mongodb connection
mongoose.connect(process.env.URL)
.then(res => {
    console.log("connection established");
})
.catch(err => {
    console.log("connection error: " + err);
})

// get api to fetch all the rules
router.get('/data', async ctx => {
    try{
        const docs = await validation.find({});
        ctx.body= docs;
    }
    catch(err){
        ctx.body = "Error: " + err.message;
    }
})

//post api to add a rule
router.post('/new', async ctx => {
    try{
        const {ruleName, regex} = ctx.request.body;
        await validation.create({ruleName,regex})
        ctx.body = "Rule posted successfully";
    }
    catch(err){
        ctx.body = "Error: " + err.message;
    }
})

//delete api to delete a rule
router.delete('/data/:id', async ctx => {
    try{
        const {id} = ctx.request.params;
        await validation.findByIdAndDelete(id);
        ctx.body = "Rules deleted successfully"
    }
    catch(err){
        ctx.body = "Error: " + err.message;
    }
})

//patch api to edit a rule
router.patch('/data/:id', async ctx => {
    try{
        const {id} = ctx.request.params;
        const {ruleName, regex} = ctx.request.body;
        await validation.findByIdAndUpdate(id,{ruleName, regex});
        ctx.body = "Rule edited successfully";
    }
    catch(err){
        ctx.body = "Error: " + err.message;
    }
})

//post api to find all the matches
router.post('/match', async ctx => {
    try{
        const {text} = ctx.request.body;
        const docs = await validation.find({});
        let match = [];
        let regex, valid;
        docs.forEach(data => {
            regex = data.regex;
            valid = text.match(regex);
            if(valid)
                match.push(data);
        })
        if(match.length > 0)
            ctx.body = match;
        else
            ctx.body = "No matching rule found for "+text;
    }
    catch(err){
        ctx.body = "Error: " + err.message;
    }
})

app.listen(process.env.PORT,() => console.log(`server is running on port ${process.env.PORT}`));