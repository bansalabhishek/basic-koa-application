const mongoose = require("mongoose");

const validationSchema = new mongoose.Schema({
    ruleName: String,
    regex: String
});

const validation = mongoose.model('Validation',validationSchema);

module.exports = validation;